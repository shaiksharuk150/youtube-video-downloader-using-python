import tkinter
import customtkinter
from pytube import YouTube

# System settings

customtkinter.set_appearance_mode("System")
customtkinter.set_default_color_theme("blue")


# App

app = customtkinter.CTk()
app.geometry("1080x720")
app.title("YouTube Video Downloader")


# Download Function

def startDownload(option):
  try:
    ytLink = link.get()
    ytObject = YouTube(ytLink,on_progress_callback=on_progress)
    if option=="high_quality":
      video = ytObject.streams.get_highest_resolution()

    elif option=="low_quality":
      video = ytObject.streams.get_lowest_resolution()

    elif option=="only_audio":
      video = ytObject.streams.get_audio_only()
    else:
      return
    
    title.configure(text=ytObject.title,text_color="white")
    finishlabel.configure(text="")
    video.download()
    
    finishlabel.configure(text="Download Complete",text_color="green")

  except:
    finishlabel.configure(text="Invalid Link !!!!!!!",text_color="red")
    

# Progress Function

def on_progress(stream,chunk,bytes_remaining):
  total_size=stream.filesize
  bytes_download = total_size - bytes_remaining
  percentage_of_completion = (bytes_download/ total_size)*100
  per = str(int(percentage_of_completion))

  progress.configure(text = per+"%")
  progress.update()

  progressBar.set(float(percentage_of_completion)/100)








# UI elements

title = customtkinter.CTkLabel(app,text="Insert YouTube Video Link below")
title.pack()


# link pasting searchbox
var_url=tkinter.StringVar()
link = customtkinter.CTkEntry(app,width=400,height=50,textvariable=var_url)
link.pack()

# Finish label

finishlabel = customtkinter.CTkLabel(app,text="")
finishlabel.pack(padx=10,pady=10)

# Percentage Label

progress = customtkinter.CTkLabel(app,text="0%")
progress.pack(padx=10,pady=10)

# Progress Bar

progressBar = customtkinter.CTkProgressBar(app,width=400)
progressBar.set(0)
progressBar.pack(padx=10,pady=10)


# Download Button

download1 = customtkinter.CTkButton(app,text="High Mp4" ,command=lambda:startDownload("high_quality"))
download1.pack(padx=10,pady=10)


download2 = customtkinter.CTkButton(app,text="low Mp4" ,command=lambda:startDownload("low_quality"))
download2.pack(padx=10,pady=10)


download3 = customtkinter.CTkButton(app,text="Audio Mp3" ,command=lambda:startDownload("only_audio"))
download3.pack(padx=10,pady=10)


# Running app

app.mainloop()